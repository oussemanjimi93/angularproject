import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private http: HttpClient) { }

  personList(){
    return this.http.get('http://127.0.0.1:8000/api/person-list');
  }
  updatePerson(id,form){
    return this.http.put("http://127.0.0.1:8000/api/person/"+id,form)
  }
  deletePerson(id){
    return this.http.delete("http://127.0.0.1:8000/api/person/"+id)
  }
  addPerson(form){
    return this.http.post("http://127.0.0.1:8000/api/person",form)
  }
}
