import { Component, OnInit } from '@angular/core';
import { PersonService } from '../services/person.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit {
  personList: any;
  editPerson: any;
  editProfileForm: FormGroup;
  addPersonForm: FormGroup
  constructor(private personservice: PersonService, private modalService: NgbModal, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.handleGetList()
    this.initForm()
    this.initAddPersonForm()
  }
  initForm() {
    this.editProfileForm = this.fb.group({
      first_name: [''],
      last_name: [''],
      email: [''],
      adresse: ['']
    });

  }
  initAddPersonForm(){
    this.addPersonForm = this.fb.group({
      first_name: ['',Validators.required],
      last_name: ['',Validators.required],
      email: ['',Validators.required],
      adresse: ['',Validators.required]
    });
  }
  handleGetList() {
    this.personservice.personList().subscribe((res: any) => {
      this.personList = res
      console.log("list person", this.personList)

    })
  }
  openAddModal(targetModal){
    this.modalService.open(targetModal)
  }
  openEditModal(targetModal, person) {
    this.modalService.open(targetModal)
    this.editPerson = person
    this.editProfileForm.patchValue({
      first_name: person.first_name,
      last_name: person.last_name,
      email: person.email,
      adresse: person.adresse

    });
  }
  editForm() {
    this.personservice.updatePerson(this.editPerson.id,this.editProfileForm.value).subscribe((res)=>{
      location.reload()
      this.modalService.dismissAll()
    })
  }

  deletePerson(id){
    this.personservice.deletePerson(id).subscribe((res)=>{
      location.reload()
    })
  }
  addPerson(){
    if (this.addPersonForm.valid){
      this.personservice.addPerson(this.addPersonForm.value).subscribe(()=>{
        location.reload()
        this.modalService.dismissAll()
      })
    }else{
      alert("Formulaire non valid")
    }
  }
}
